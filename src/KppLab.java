import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class KppLab {
    static private List<NewspaperInfo> newspaperInfo1;
    static private List<NewspaperInfo> newspaperInfo2;

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        System.out.print("Enter newspapers info filename(I): ");
        String filename1 = in.next();
        newspaperInfo1 = readFromFile(filename1);

        System.out.print("Enter newspapers info filename(II): ");
        String filename2 = in.next();
        newspaperInfo2 = readFromFile(filename2);

        while(true){
            int action = showMenu();

            switch (action){
                case 1 -> printList(newspaperInfo1);
                case 2 -> printList(newspaperInfo2);
                case 3 -> printList(mergeAndDescendingSort(newspaperInfo1, newspaperInfo2));
                case 4 -> printNewspaperMap(createMapOfNewspapers(mergeLists(newspaperInfo1, newspaperInfo2)));
                case 5 -> printList(swapOddAndEvenNames(mergeLists(newspaperInfo1, newspaperInfo2)));
                case 6->{
                    System.out.print("Enter mim circulation: ");
                    printList(selectCirculationNotLessThan(mergeLists(newspaperInfo1, newspaperInfo2), in.nextInt()));
                }
                case 7 -> printFrequencyMap(createFrequencyCirculationMap(mergeLists(newspaperInfo1, newspaperInfo2)));
                case 8 -> printList(mergeLists(newspaperInfo1, newspaperInfo2));
            }
        }
    }

    private static void printNewspaperMap(Map<String, Set<String>> map){
        for(var v: map.entrySet()){
            System.out.print(v.getKey() + ": ");
            for(var name: v.getValue()) {
                System.out.print(name + " ");
            }
            System.out.println();
        }
    }

    private static void printFrequencyMap(Map<Integer, Long> map){
        System.out.println("Circulation : Frequency");
        for(var v: map.entrySet()){
            System.out.println("\t " + v.getKey() + " : \t " + v.getValue());

        }
    }

    private static Map<String, Set<String>> createMapOfNewspapers(List<NewspaperInfo> list){
        return list.stream().collect(
                Collectors.groupingBy(
                NewspaperInfo::getPublisher,
                        Collectors.mapping(
                                NewspaperInfo::getName,
                                Collectors.toSet()
                        )
                ));
    }

    private static Map<Integer, Long> createFrequencyCirculationMap(List<NewspaperInfo> list){
        return list.stream().collect(
                Collectors.groupingBy(
                NewspaperInfo::getCirculation,
                Collectors.counting()
        ));
    }

    private static List<NewspaperInfo> selectCirculationNotLessThan(List<NewspaperInfo> list, int minCirculation){
        return list.stream()
                .filter(v-> v.getCirculation() >= minCirculation)
                .collect(Collectors.toList());
    }

    private static List<NewspaperInfo> swapOddAndEvenNames(List<NewspaperInfo> list){
        List<NewspaperInfo> result = new ArrayList<>(list);

        for(int i = 1; i < list.size(); i+= 2){
            String tmp = list.get(i).getName();
            list.get(i).setName(list.get(i -1).getName());
            list.get(i -1).setName(tmp);
        }
        return result;
    }

    private static int showMenu(){
        System.out.println("Select action: ");
        System.out.println("\t1) Print first file data.");
        System.out.println("\t2) Print second file data.");
        System.out.println("\t3) Print merged and descending sorted data.");
        System.out.println("\t4) Show map with publisher as key and newspapers sets as values(for first file).");
        System.out.println("\t5) Swap odd and even names.");
        System.out.println("\t6) Remove newspapers with circulation less than...");
        System.out.println("\t7) Show frequency characteristic.");
        System.out.println("\t8) Print merged data.");
        System.out.print("... enter action: ");

        Scanner in = new Scanner(System.in);
        int action = in.nextInt();
        while(action < 1 || action > 8){
            System.out.print("... enter listed action: ");
            action = in.nextInt();
        }
        return action;
    }

    private static List<NewspaperInfo> mergeLists(List<NewspaperInfo> a, List<NewspaperInfo> b){
        ArrayList<NewspaperInfo> result = new ArrayList<>(a);
        result.addAll(b);

        return Collections.unmodifiableList(result);
    }

    private static List<NewspaperInfo> mergeAndDescendingSort(List<NewspaperInfo> a, List<NewspaperInfo> b){
        List<NewspaperInfo> result = new ArrayList<>(mergeLists(a,b));

        result.sort((x, y) -> -x.getName().compareTo(y.getName()));

        return Collections.unmodifiableList(result);
    }

    private static void printList(List<NewspaperInfo> list){
        for(var v: list){
            System.out.println(v.toString());
        }
    }

    private static List<NewspaperInfo> readFromFile(String fileName){
        List<NewspaperInfo> result = new ArrayList<>();

        try {
            File file = new File(fileName);
            Scanner reader = new Scanner(file);
            while (reader.hasNext()) {

                String name = reader.next();
                int foundationYear = reader.nextInt();
                int circulation = reader.nextInt();
                int frequencyPerYear = reader.nextInt();
                String publisher = reader.next();

                result.add(new NewspaperInfo(name, foundationYear, circulation, frequencyPerYear, publisher));

            }
            reader.close();
        } catch (FileNotFoundException e) {
            System.out.println("File " + fileName + " not found.");
        }
        return Collections.unmodifiableList(result);
    }
}
