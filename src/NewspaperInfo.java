public class NewspaperInfo {
   private String name;
   private int foundationYear;
   private int circulation;
   private int frequencyPerYear;
   private String publisher;

   public NewspaperInfo(String name, int foundationYear, int circulation, int frequencyPerYear, String publisher){
      this.name = name;
      this.foundationYear = foundationYear;
      this.circulation = circulation;
      this.frequencyPerYear = frequencyPerYear;
      this.publisher = publisher;
   }

   public int getCirculation() {
      return circulation;
   }

   public int getFoundationYear() {
      return foundationYear;
   }

   public int getFrequencyPerYear() {
      return frequencyPerYear;
   }

   public String getName() {
      return name;
   }

   public String getPublisher() {
      return publisher;
   }

   public void setName(String name) {
      this.name = name;
   }

   @Override
   public String toString() {
      return  name
              + " \t(founded in " + foundationYear
              + ", \tcirculation: " + circulation
              + ", \tfrequency: " + frequencyPerYear + " per year"
              + ", \tpublisher: " + publisher + ")";

   }

}
